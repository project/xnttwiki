<?php

namespace Drupal\xnttwiki\Plugin\ExternalEntities\StorageClient;

use Drupal\Core\Form\FormStateInterface;
use Drupal\external_entities\Plugin\ExternalEntities\StorageClient\RestClient;

/**
 * Mediawiki external entities storage client plugin based on a REST API.
 *
 * @StorageClient(
 *   id = "wiki",
 *   label = @Translation("Wiki"),
 *   description = @Translation("Retrieves external entities from a Mediawiki API (Wikipedia).")
 * )
 */
class Wiki extends RestClient {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state
  ) {
    $form = parent::buildConfigurationForm($form, $form_state);

    // Adjust default REST form.
    $form_override = [
      'endpoint' => [
        '#title' => $this->t('Wiki API URL'),
        '#description' => $this->t(
          'Should be something similar to "<code>https://www.example.org/w/api.php</code>".'
        ),
        '#default_value' => $this->configuration['endpoint'] ?: "https://en.wikipedia.org/w/api.php",
        '#attributes' => [
          'placeholder' => $this->t("ex.: https://en.wikipedia.org/w/api.php"),
        ],
      ],
      'endpoint_options' => [
        'cache' => [
          '#type' => 'hidden',
          '#value' => FALSE,
        ],
        'single' => [
          '#type' => 'hidden',
          '#value' => '',
        ],
        'count' => [
          '#type' => 'textfield',
          '#title' => $this->t('Total number of wiki pages'),
          '#required' => FALSE,
          '#description' => $this->t('Only use this field if you are not using a "search" list type of query. Otherwise, "search" list type supports a "srinfo=totalhits" parameter that provides the number of maching pages.'),
          '#default_value' => $this->configuration['endpoint_options']['count'] ?? '',
          '#attributes' => [
            'placeholder' => $this->t('ex.: "25000"'),
          ],
        ],
        'count_mode' => [
          '#type' => 'hidden',
          '#value' => 'entities',
        ],
        'limit_qcount' => [
          '#type' => 'hidden',
          '#value' => '200',
        ],
        'limit_qtime' => [
          '#type' => 'hidden',
          '#value' => '1',
        ],
      ],
      'response_format' => [
        '#type' => 'hidden',
        '#value' => 'json',
      ],
      'data_path' => [
        '#open' => FALSE,
        '#description' => NULL,
        'list' => [
          '#type' => 'hidden',
          '#value' => '$.query[' . implode(',', array_keys(static::getListTypePrefix())) . '].*',
        ],
        'single' => [
          '#type' => 'hidden',
          '#value' => '$.query.pages.*',
        ],
        'keyed_by_id' => [
          '#type' => 'hidden',
          '#value' => FALSE,
        ],
        // Do not warn if not set yet.
        'count' => [
          '#default_value' => $this->configuration['data_path']['count'] ?: '$.query.searchinfo.totalhits',
          '#attributes' => [
            'placeholder' => $this->t('ex.: $.query.searchinfo.totalhits'),
          ],
          '#description' => NULL,
        ],
      ],
      'pager' => [
        '#open' => FALSE,
        // Pager limit is auto-set using '*limit' parameters.
        // Default Wiki limit is 10 but it's better for Drupal to use a 50 elements
        // basis (1 query/page).
        'default_limit' => [
          '#type' => 'hidden',
          '#value' => static::DEFAULT_PAGE_LENGTH,
        ],
        'page_parameter' => [
          '#attributes' => [
            'placeholder' => $this->t('ex.: sroffset'),
          ],
        ],
        'page_parameter_type' => [
          '#type' => 'hidden',
          '#value' => 'startitem',
        ],
        'page_size_parameter' => [
          '#attributes' => [
            'placeholder' => $this->t('ex.: srlimit'),
          ],
        ],
        'page_size_parameter_type' => [
          '#type' => 'hidden',
          '#value' => 'pagesize',
        ],
        'page_start_one' => [
          '#type' => 'hidden',
          '#value' => FALSE,
        ],
        'always_query' => [
          '#type' => 'hidden',
          '#value' => TRUE,
        ],
      ],
      'api_key' => [
        '#type' => NULL,
        'type' => [
          '#type' => 'hidden',
          '#value' => 'none',
        ],
        'key' => [
          '#type' => 'hidden',
          '#value' => '',
        ],
        'header_name' => [
          '#type' => 'hidden',
          '#value' => '',
        ],
      ],
      'http' => [
        '#type' => NULL,
        'headers' => [
          '#type' => 'hidden',
          '#value' => '',
        ],
      ],
      'parameters' => [
        '#open' => TRUE,
        'list' => [
          '#attributes' => [
            'placeholder' => $this->t("ex.:\naction=query\nformat=json\nlist=search\nsrsearch=Haarlem\nsrprop=categorysnippet\nsrinfo=totalhits"),
          ],
        ],
        'list_param_mode' => [
          '#type' => 'hidden',
          '#value' => 'query',
        ],
        'single' => [
          '#attributes' => [
            'placeholder' => $this->t("ex.:\naction=query\nformat=json\nprop=extracts"),
          ],
        ],
        'single_param_mode' => [
          '#type' => 'hidden',
          '#value' => 'query',
        ],
        'default_params' => [
          '#type' => 'item',
          '#markup' => $this->t('<b>NOTE:</b> The following parameters are automatically set: <code>action=query</code> and <code>format=json</code>. You should provide at least a "list" parameter with its corresponding required arguments. It is better to use "<a href="https://www.mediawiki.org/wiki/API:Search">search</a>" list type which provides paging (<code>srlimit</code>, <code>sroffset</code> and <code>srinfo=totalhits</code>) otherwise, you will have hard time to simulate a correct paging or will be limited to one page of results. See <a href="https://www.mediawiki.org/wiki/API:Lists">MediaWiki List API documentation</a> for details.'),
        ],
      ],
      'filtering' => [
        '#type' => NULL,
        'drupal' => [
          '#type' => 'hidden',
          '#value' => FALSE,
        ],
        'basic' => [
          '#type' => 'hidden',
          '#value' => FALSE,
        ],
        'basic_fields' => [
          '#type' => 'hidden',
          '#value' => '',
        ],
        'list_support' => [
          '#type' => 'hidden',
          '#value' => 'none',
        ],
        'list_join' => [
          '#type' => 'hidden',
          '#value' => ',',
        ],
      ],
    ];

    // Merge forms.
    $form = $this->overrideForm(
      $form,
      $form_override,
      [
        '#type' => 'hidden',
      ]
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $endpoint = $form_state->getValue('endpoint', '');
    $form_state->setValue(['endpoint_options', 'single'], $endpoint);

    $parameters = $form_state->getValue('parameters');
    foreach (['single', 'list'] as $type) {
      // Ensure query action and JSON response format are set.
      // First, clear settings for action and format.
      $parameters[$type] = preg_replace('#(?<=^|\n)(?:action|format)=[^\n]*(?:\n|$)#', '', $parameters[$type]);
      $parameters[$type] = "action=query\nformat=json\n" . $parameters[$type];
    }
    // Ensure we got the id field in single query.
    if (FALSE === strpos($parameters['single'], 'pageids={id}')) {
      $parameters['single'] = preg_replace('#(?<=^|\n)(?:pageids)=[^\n]*(?:\n|$)#', '', $parameters['single']);
      $parameters['single'] = "pageids={id}\n" . $parameters['single'];
    }
    $list_param = &$parameters['list'];

    // Check for list type.
    if (!preg_match('#(?:^|\n)list=([-:\w]+)#', $list_param, $list_matches)) {
      // Warn the list enpoint URL may not be correct.
      if (!$form_state->isRebuilding()) {
        \Drupal::messenger()->addWarning('You should provide a "list=<list_type>" parameter in the "List parameters" section.');
      }
    }
    elseif ('search' == $list_matches[1]) {
      // If 'search' type, make sure we got srinfo.
      $list_param = preg_replace('#(?<=^|\n)(?:srinfo)=[^\n]*\n?#', '', $list_param);
      $list_param .= "\nsrinfo=totalhits";
    }
    $prefix = '';
    if (!empty($list_matches[1])) {
      $prefix = static::getListTypePrefix($list_matches[1]);
    }

    // Check for pager config.
    $pager = $form_state->getValue('pager');
    if (preg_match('#(?:^|\n)\w{0,3}limit=(\d+)#', $list_param, $matches)) {
      $pager['default_limit'] = $matches[1];
    }
    elseif ($prefix) {
      // Add default limit (for Drupal).
      $list_param =
        trim($list_param, "\n")
        . "\n{$prefix}limit="
        . static::DEFAULT_PAGE_LENGTH
        . "\n";
    }

    if ($prefix) {
      // Remove pager offset from parameters and add offset and limit to the
      // pager config.
      $list_param = preg_replace(
        '#(?<=^|\n)' . $prefix . 'offset=[^\n]*\n?#',
        '',
        $list_param
      );
      if (empty($pager['page_parameter'])) {
        $pager['page_parameter'] = $prefix . 'offset';
      }
      if (empty($pager['page_size_parameter'])) {
        $pager['page_size_parameter'] = $prefix . 'limit';
      }
    }

    $form_state->setValue('parameters', $parameters);
    $form_state->setValue('pager', $pager);
    parent::validateConfigurationForm($form, $form_state);
  }

  /**
   * Returns Wikipedia (API) prefix for the given list type.
   *
   * @param string|null $list_type
   *   The list type. Ex.: 'categorymembers'. If not set, returns an array with
   *   all list types as key and their associated prefixes as values.
   *
   * @return string|array:
   *   The prefix used in the API or an empty string if not available. If
   *   $list_type is not set, returns an array with all list types as key and
   *   their associated prefixes as values.
   */
  public static function getListTypePrefix(string $list_type = NULL) :string|array {
    static $prefixes = [
      'search'              => 'sr',
      'allcategories'       => 'ac',
      'alldeletedrevisions' => 'adr',
      'allfileusages'       => 'af',
      'allimages'           => 'ai',
      'alllinks'            => 'al',
      'allpages'            => 'ap',
      'allredirects'        => 'ar',
      'allrevisions'        => 'arv',
      'alltransclusions'    => 'at',
      'allusers'            => 'au',
      'backlinks'           => 'bl',
      'blocks'              => 'bk',
      'categorymembers'     => 'cm',
      'deletedrevs'         => 'dr',
      'embeddedin'          => 'ei',
      'exturlusage'         => 'eu',
      'filearchive'         => 'fa',
      'imageusage'          => 'iu',
      'iwbacklinks'         => 'iwbl',
      'langbacklinks'       => 'lbl',
      'logevents'           => 'le',
      'mystashedfiles'      => 'msf',
      'pagepropnames'       => 'ppn',
      'pageswithprop'       => 'pwp',
      'prefixsearch'        => 'ps',
      'protectedtitles'     => 'pt',
      'querypage'           => 'qp',
      'random'              => 'rn',
      'recentchanges'       => 'rc',
      'search'              => 'sr',
      'tags'                => 'tg',
      'usercontribs'        => 'uc',
      'users'               => 'us',
      'watchlist'           => 'wl',
      'watchlistraw'        => 'wr',
    ];
    if (!isset($list_type)) {
      return $prefixes;
    }
    return $prefixes[$list_type] ?? '';
  }

}
