External Entities Mediawiki plugin
**********************************

Mediawiki storage client enables the use of Mediawiki content on a Drupal site.

===============================

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------

Mediawiki external entities storage client plugin enables the use of external
Mediawiki (Wikipedia) content into a Drupal site as external entities. It works
based on Mediawiki API services and external entities REST client.

This module was originally part of External Entities module as a built-in plugin
but has been separated from the core module since External Entities v3 to
become a regular external entities plugin on its own like other REST plugins.

REQUIREMENTS
------------

This module requires the following modules:

 * [External Entities](https://www.drupal.org/project/external_entities)

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

The module has no menu or modifiable global settings. There is no configuration.
When enabled, the module will add a new storage client for external entity
types. Then, when you create a new external entity type, you can select the
"Wiki" plugin and have access to settings specific to the new external entity.

MAINTAINERS
-----------

Current maintainers:
 * Valentin Guignon (guignonv) - https://www.drupal.org/u/guignonv
